#include <semaphore.h>


void bsem_init(bsem_t p, int value)
{
	pthread_mutex_init(&(p->mutex), NULL);
	pthread_cond_init(&(p->cond), NULL);
	p->v = value;
}

void post(bsem_t p)
{
	pthread_mutex_lock(&p->mutex);
	p->v = 1;
	pthread_cond_signal(&p->cond);
	pthread_mutex_unlock(&p->mutex);
}

void post_all(bsem_t p)
{
	pthread_mutex_lock(&p->mutex);
	p->v = 1;
	pthread_cond_broadcast(&p->cond);
	pthread_mutex_unlock(&p->mutex);
}

void bsem_wait(bsem_t p)
{
	pthread_mutex_lock(&p->mutex);

	while (p->v != 1) {
		pthread_cond_wait(&p->cond, &p->mutex);
	}

	p->v = 0;
	pthread_mutex_unlock(&p->mutex);
}
