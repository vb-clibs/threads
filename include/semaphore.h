#ifndef _SEMAPHORE_
#define _SEMAPHORE_


#include <pthread.h>


#define BSEM_SIZE sizeof(struct _bsem)


struct _bsem {
	int v;
	pthread_cond_t cond;
	pthread_mutex_t mutex;
};

typedef struct _bsem * bsem_t;
 

void bsem_init(bsem_t p, int value);
void bsem_post(bsem_t p);
void bsem_post_all(bsem_t p);
void bsem_wait(bsem_t p);


struct _bsem_namespace
{
	void (*init)(bsem_t, int);
	void (*signal)(bsem_t);
	void (*broadcast)(bsem_t);
	void (*wait)(bsem_t);
};


static const struct _bsem_namespace
BinarySemaphore = {
	.init = &bsem_init,
	.signal = &bsem_post,
	.broadcast = &bsem_post_all,
	.wait = &bsem_wait,
};

#endif
